In order to test the weather API, following steps should be done:

1)
    - Python 3 installed (I have Python 3.7)
    - instaled *pip* package manager

2)
    Further steps are:
    - go to project folder (\weather-test)
    - run `pip install -r requirements.txt` (you would better do it in virtual env )

    The 3rd party libraries will be installed:
    - pytest (test framework)
    - weather-api (api around http service with actual data about weather)
3)
    After that you will need to find *test/weather_config.py* and modify the path for
    *weather_csv_path* variable so the path for weather.csv file exists
    Also, there is a list of currently supported cities. This list should be modified if weather.csv has info about some other cities, not just San Francisco and Hoboken

To run tests run from the command line:
`pytest tests/ --city="San Francisco"`
or
`pytest tests/ --city="Hoboken"`

NOTE:
`--city %city%` argument is required in order to run the test
